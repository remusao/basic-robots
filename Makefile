# Targets to delete in Distclean rule
DISTCLEAN = saves
# Targets to delete in Clean rule
CLEAN = output.ps output.dot src/__pycache__
# Targets to tar in dist rule
DIST = doc res src AUTHORS basic-robots.py Makefile README.md TODO

NAME = basic-robots
VERSION = 0.42

ENTRY_POINT = src/main.py

SUPPORTED_BINARY = python2.7 python2.6 python2.5 python2.4
# Compute the value of the binary
BINARY = $(shell for binary in ${SUPPORTED_BINARY}; do 	\
		if [ -e /usr/bin/$$binary ] ; then 	\
			echo $$binary;			\
			break;				\
		fi;					\
	done;)

# Check if a binary was found
ifndef BINARY
    $(error No compatible binary was found)
endif

.PHONY: doc

run:
	@echo 'Running -> ${NAME}'
	@echo 'Version : ${VERSION}'
	@echo 'Python binary : ${BINARY}'
	@${BINARY} ${ENTRY_POINT}

viewImport:
	@${BINARY} src/modelViewer/modelViewer.py importScan
	@dot output.dot -Tps -o output.ps

viewInheritance:
	@${BINARY} src/modelViewer/modelViewer.py inheritanceScan
	@dot output.dot -Tps -o output.ps

doc:
	@make -C doc/

clean:
	@find . -name "*.pyc" -delete
	@make -C doc/ clean
	@rm -frv ${CLEAN}

distclean: clean
	@make -C doc/ distclean
	@rm -frv ${DISTCLEAN}
