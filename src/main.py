##
## main.py for BASIC-RoBots in /home/surply_p
## 
## Made by Pierre Surply
## <pierre.surply@gmail.com>
## 
## Started on  Wed May  9 17:39:28 2012 Pierre Surply
# Last update Sun Jul 15 15:12:30 2012 Pierre Surply
##

import os

import display
import events
import game
import dashboard
import worldmap
import menu

class Main:
    def __init__(self):
        self.display = display.Display()
        self.input = events.Input()
        self.dashboard = None
        self.menu = menu.Menu(self.input, self.display)
        self.state = 0
        self.quit = False

    def run(self):
        while not self.input.quit and not self.quit:
            self.input.update(self.display)
            if self.state == 0:
                cmd = self.menu.update(self.display, self.input)
                if cmd != None:
                    self.menu_command(cmd)
            elif self.state == 1:
                if self.dashboard.update(self.display, self.input):
                    self.dashboard.worldmap.build_env((self.dashboard.select_x,\
                                                          self.dashboard.select_y), \
                                                          self.input)
                    self.game = game.Game(self.input, \
                                              self.dashboard.worldmap.env[self.dashboard.select_x]\
                                              [self.dashboard.select_y])
                    self.state = 2
            elif self.state == 2:
                if self.game.update(self.display, self.input):
                    self.state = 1
        self.dashboard.worldmap.save()

    def menu_command(self, cmd):
        if cmd[0] == 0:
            self.dashboard = dashboard.Dashboard(self.new_game(cmd[1]), self.input)
            self.state = 1
        elif cmd[0] == 1:
            self.dashboard = dashboard.Dashboard(self.load_game(cmd[1]), self.input)
            self.state = 1
        elif cmd[0] == 2:
            self.quit = True


    def new_game(self, path):
        os.makedirs("saves/" + path)
        os.mkdir("saves/" + path + "/env")
        os.mkdir("saves/" + path + "/robots")
        wm = worldmap.WorldMap(path)
        wm.save()
        return wm


    def load_game(self, path):
        wm = worldmap.WorldMap(path)
        wm.load(self.input)
        return wm


if __name__ == "__main__":
    Main().run()
