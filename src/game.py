##
## game.py for BASIC-RoBots in /home/surply_p
## 
## Made by Pierre Surply
## <pierre.surply@gmail.com>
## 
## Started on  Thu Jun 28 15:31:47 2012 Pierre Surply
## Last update Mon Jul  2 18:37:42 2012 Pierre Surply
##

import pygame
from pygame.locals import *

import display
import events
import env
import worldmap

class Game:
    def __init__(self, events, env):
        self.env = env
        self.events = events
        self.select_x, self.select_y = env.get_pos_robot(0)
        self.select_robot = None
        self.events.rec_char = False
        self.goto_dashboard = False
        
    def update(self, display, events):
        if events.get_key_once(K_ESCAPE):
            if self.select_robot != None:
                self.select_robot = None
                self.events.rec_char = False
            else:
                self.goto_dashboard = True
        if events.get_key_once(K_TAB):
            if events.key[K_LCTRL]:
                self.incr_select_robot(-1)
            else:
                self.incr_select_robot(1)
        self.env.update_robots(self.select_robot, display, events)
        if self.select_robot != None:
            self.select_x, self.select_y = self.env.get_pos_robot(self.select_robot)
            display.render_terminal()
            self.env.render(display.window, \
                                (self.select_x, self.select_y, \
                                     display.window.get_width() / 64, \
                                     display.window.get_height() / 32), \
                                (display.window.get_width()/2, 0))
        else:
            self.handle_events(events)
            self.env.render(display.window, \
                                (self.select_x, self.select_y, \
                                     display.window.get_width() / 32, \
                                     display.window.get_height() / 32), \
                                (0,0))
        display.flip()
        return self.goto_dashboard
        

    def handle_events(self, events):
        if events.get_key_once(K_RETURN):
            for i in range(len(self.env.robots)):
                if self.env.robots[i].get_pos() == (self.select_x, \
                                                      self.select_y):
                    self.select_robot = i
                    self.events.rec_char = True
        if events.key[K_LCTRL]:
            arrows = (events.key[K_UP], \
                          events.key[K_DOWN],\
                          events.key[K_LEFT],\
                          events.key[K_RIGHT])
        else:
            arrows = (events.get_key_once(K_UP), \
                          events.get_key_once(K_DOWN),\
                          events.get_key_once(K_LEFT),\
                          events.get_key_once(K_RIGHT))
        if arrows[0] and self.select_y > 0:
            self.select_y -= 1
        if arrows[1] and self.select_y < self.env.size-1:
            self.select_y += 1
        if arrows[2] and self.select_x > 0:
            self.select_x -= 1
        if arrows[3] and self.select_x < self.env.size-1:
            self.select_x += 1

    def incr_select_robot(self, incr):
        self.events.rec_char = True
        if self.select_robot == None:
            self.select_robot = 0
        else:
            self.select_robot += incr
            if self.select_robot > len(self.env.robots)-1:
                self.select_robot = 0
