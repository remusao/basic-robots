##
## display.py for BASIC-RoBots in /home/surply_p
## 
## Made by Pierre Surply
## <pierre.surply@gmail.com>
## 
## Started on  Wed May  9 17:19:46 2012 Pierre Surply
# Last update Sun Jul  8 23:47:09 2012 Pierre Surply
##

import pygame
from pygame.locals import *

import surface

class Display:
    def __init__(self):
        pygame.init()
        pygame.display.set_caption("BASIC-RoBots")
        self.set_display((640, 480))
        self.set_bg()
        self.set_tile()
        self.ascii = pygame.image.load("res/img/font.png").convert_alpha()
        self.ascii_wb = surface.cut_surface(self.ascii, (8, 12))
        self.ascii_bw = surface.cut_surface(surface.change_color(self.ascii, \
                                                                     pygame.Color(0, 0, 0, 255),\
                                                                     pygame.Color(255, 255, 255, 255)),\
                                                (8, 12))
        self.ascii_gg = surface.cut_surface(surface.change_color(self.ascii, \
                                                                     pygame.Color(0, 255, 0, 255)),\
                                                (8, 12))
        self.ascii_g = surface.cut_surface(surface.change_color(self.ascii, \
                                                                     pygame.Color(0, 100, 0, 255)),\
                                               (8, 12))
        self.ascii_b = surface.cut_surface(surface.change_color(self.ascii, \
                                                                    pygame.Color(0, 100, 200, 255)),\
                                               (8, 12))
        self.ascii_r = surface.cut_surface(surface.change_color(self.ascii, \
                                                                    pygame.Color(100, 0, 0, 255)),\
                                               (8, 12))
        self.ascii_rr = surface.cut_surface(surface.change_color(self.ascii, \
                                                                     pygame.Color(255, 0, 0, 255)),\
                                               (8, 12))
        self.ascii_rb = surface.cut_surface(surface.change_color(self.ascii, \
                                                                     pygame.Color(200, 00, 0, 255),\
                                                                     pygame.Color(255, 255, 255, 255)),\
                                               (8, 12))
        self.ascii_gb = surface.cut_surface(surface.change_color(self.ascii, \
                                                                     pygame.Color(0, 100, 0, 255),\
                                                                     pygame.Color(255, 255, 255, 255)),\
                                               (8, 12))
    def set_display(self, size):
        self.window = pygame.display.set_mode(size, pygame.DOUBLEBUF | pygame.RESIZABLE)

    def set_bg(self):
        self.bg = pygame.Surface(self.window.get_size())
        self.bg.fill(pygame.Color(0, 0, 0))
        self.env = pygame.Surface((self.window.get_width() / 2, self.window.get_height()))

    def set_tile(self):
        self.tile = [None] * (self.window.get_height() / 12)
        self.tile = [[None] * (self.window.get_width() / 8) for i in self.tile]

    def set_tile_fullscreen(self):
        self.term_fullscreen = not(self.term_fullscreen)

    def render_terminal(self, fullscreen=False):
        self.window.blit(self.bg, (0, 0))
        if fullscreen:
            w = len(self.tile[0])
        else:
            w = len(self.tile[0])/2
        for y in range(len(self.tile)):
            for x in range(w):
                if self.tile[y][x] != None:
                    self.window.blit(self.tile2surface(self.tile[y][x]), (x * 8, y * 12))

    def flip(self):
        pygame.display.flip()

    def print_char(self, (x, y), tile):
        if len(self.tile) > y and len(self.tile[0]) > x:
            self.tile[y][x] = tile
            return True
        else:
            return False

    def resize(self, size):
        if size[0] >= 80 and size[1] >= 120: 
            self.set_display(size)
            self.set_bg()
            self.set_tile()

    def tile2surface(self, tile):
        # MSB
        # 8 bits : id
        #          - 0x00 : Character white on black
        #          - 0x01 : Character black on white
        #          - 0x02 : Green
        #          - 0x03 : Dark green
        #          - 0x04 : Blue
        #          - 0x05 : Dark red
        #          - 0x06 : Red
        #          - 0x07 : Green on white
        #          - 0x08 : Red on white
        # 8 bits : data
        # LSB
        if isinstance(tile, int):
            tile_t = tile >> 8
            data = tile & 0xFF
            if tile_t == 0x00:
                return self.ascii_wb[data]
            elif tile_t == 0x01:
                return self.ascii_bw[data]
            elif tile_t == 0x02:
                return self.ascii_gg[data]
            elif tile_t == 0x03:
                return self.ascii_g[data]
            elif tile_t == 0x04:
                return self.ascii_b[data]
            elif tile_t == 0x05:
                return self.ascii_r[data]
            elif tile_t == 0x06:
                return self.ascii_rr[data]
            elif tile_t == 0x07:
                return self.ascii_gb[data]
            elif tile_t == 0x08:
                return self.ascii_rb[data]
            else:
                return self.ascii_wb[0]
        else:
            return self.ascii_bw[0]

    def draw_frame(self, (x, y, w, h), border):
        for i in range(w):
            self.print_char((x+i, y), border)
        for i in range(w):
            self.print_char((x+i, y+h), border)
        for i in range(h):
            self.print_char((x, y+i), border)
        for i in range(h+1):
            self.print_char((x+w, y+i), border)
