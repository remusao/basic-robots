##
## worldmap.py for BASIC-RoBots in /home/surply_p
## 
## Made by Pierre Surply
## <pierre.surply@gmail.com>
## 
## Started on  Thu Jun 28 20:42:21 2012 Pierre Surply
## Last update Mon Jul 23 12:03:34 2012 Pierre Surply
##

import os
import pygame
from pygame.locals import *

import surface
import savable
import noise
import env
from robot import mothership
from robot import woodcutter
from robot import tipper
from robot import thermalpowerstation

class WorldMap(savable.Savable):
    biomes = [((0,0,255,255), 0, "Sea", 0),\
                  ((100,100,0,255), 1, "Beach", 2),\
                  ((0,255,0,255),2, "Plains", 0),\
                  ((10,10,10,255),3, "Montainous", 2),\
                  ((250,250,250,255),5,"Snowy", 3),\
                  ((0,250,0,255),6, "Forest", 1),\
                  ((100,0,100,255),7, "Corruption", 4),\
                  ((255,0,100,255),8, "Meteorite", 3),\
                  ((0,0,100,255),9, "Deep sea", 2),\
                  ((255,255,0,255),10, "Montainous", 2),\
                  ((255,0,0,255),4, "Volcanic", 4)]
    size = 32
    s = [(0.2, 9),\
             (0.5, 0),\
             (0.55, 1),\
             (0.555,7),\
             (0.56, 8),\
             (0.65, 2),\
             (0.75, 6),\
             (0.83, 3),\
             (0.85, 10),\
             (0.9, 5),\
             (1.0, 4)]

    def __init__(self, path):
        self.path = path
        self.sprite_biomes = surface.cut_surface(pygame.image.load("res/img/biomes.png").convert_alpha(), (8, 8))
        self.sprite_biomes_maxi = surface.cut_surface(pygame.image.load("res/img/biomes_max.png").convert_alpha(), (16, 16))
        self.cursor = pygame.image.load("res/img/biome_cursor.png").convert_alpha()
        self.cursor_max = pygame.image.load("res/img/biome_cursor_max.png").convert_alpha()
        self.init_tile()
        self.build()

    def init_tile(self):
        self.tile = [0] * self.size
        self.tile = [[0] * self.size for i in self.tile]
        self.env = [None] * self.size
        self.env = [[None] * self.size for i in self.tile]
        
        
    def render(self, surface, (dx,dy), (sx, sy)):
        for y in range(self.size):
            for x in range(self.size):
                surface.blit(self.sprite_biomes[self.tile[x][y]], (x*8+dx, y*8+dy))
        surface.blit(self.cursor, (sx*8-1+dx, sy*8-1+dy))
        if self.env[sx][sy] != None:
            self.env[sx][sy].render_mini(surface, (dx-1, (self.size*8) + dy+2))
        for y in range(max(0,sy - 4), min(sy + 4, self.size)):
            for x in range(max(0,sx - 4), min(sx + 4, self.size)):
                surface.blit(self.sprite_biomes_maxi[self.tile[x][y]], \
                                 ((x-(sx-4))*16+128+dx+1, \
                                      (y-(sy-4))*16+256+dy+2))
        surface.blit(self.cursor_max, (dx + 191, dy + 319))

    def build(self):
        self.init_tile()
        n = noise.Noise(33, 33, 4, 8)
        for y in range(self.size):
            for x in range(self.size):
                biome = self.get_rndtile(x, y, n)
                self.tile[x][y] = biome

    def get_rndtile(self, x, y, noise):
        val = noise.smooth_noise(x, y, 0.4)
        for i in self.s:
            if val < i[0]:
                return i[1]
        return self.s[0][1]

    def build_env(self, (x, y), events):
        if self.env[x][y] == None:
            self.env[x][y] = env.Env(self.path, \
                                         self.tile[x][y],\
                                         (x, y), events)


    def load(self, events):
        self.load_img("saves/" + self.path + "/env/worldmap.bmp",\
                          self.size,\
                          self.tile,\
                          self.color2tile)
        for y in range(self.size):
            for x in range(self.size):
                if os.path.isfile("saves/" + self.path + "/env/" + str(x) + "_" + str(y) + ".bmp"):
                    self.env[x][y] = env.Env(self.path, \
                                                 self.tile[x][y],\
                                                 (x, y), events)

        for i in os.listdir("saves/" + self.path + "/robots"):
            part = i.split("_")
            coord = [int(x) for x in part[1].split("-")]
            if part[0] == "Mothership":
                r = mothership.Mothership(False,\
                                              self.path, \
                                              i,\
                                              self.env[coord[0]][coord[1]], \
                                              (0, 0),\
                                              events)
            elif part[0] == "Woodcutter":
                r = woodcutter.Woodcutter(False,\
                                              self.path, \
                                              i,\
                                              self.env[coord[0]][coord[1]], \
                                              (0, 0),\
                                              events)
            elif part[0] == "Tipper":
                r = tipper.Tipper(False,\
                                       self.path, \
                                       i,\
                                       self.env[coord[0]][coord[1]], \
                                       (0, 0),\
                                       events)
            elif part[0] == "TPS":
                r = thermalpowerstation.ThermalPowerStation(False,\
                                                                self.path, \
                                                                i,\
                                                                self.env[coord[0]][coord[1]], \
                                                                (0, 0),\
                                                                events)
            self.env[coord[0]][coord[1]].robots.append(r)

    def save(self):
        self.save_img("saves/" + self.path + "/env/worldmap.bmp",\
                          self.size,\
                          self.tile,\
                          self.tile2color)
        for y in range(self.size):
            for x in range(self.size):
                if self.env[x][y] != None:
                    self.env[x][y].save()

    def tile2color(self, (x, y)):
        n = self.tile[x][y]
        for i in self.biomes:
            if n == i[1]:
                return pygame.Color(i[0][0], \
                                        i[0][1], \
                                        i[0][2],\
                                        i[0][3])

    def color2tile(self, color):
        for i in self.biomes:
            if color == i[0]:
                return i[1]
