##
## input.py for BASIC-RoBots.py in /home/surply_p
## 
## Made by Pierre Surply
## <pierre.surply@gmail.com>
## 
## Started on  Wed May  9 17:54:23 2012 Pierre Surply
## Last update Sun Jul 29 14:25:38 2012 Pierre Surply
##

import pygame
from pygame.locals import *

from collections import deque

class Input:
    def __init__(self):
        pygame.key.set_repeat(500, 30)
        self.key = [0]*1000
        self.mouse = (0,0)
        self.mouserel = (0,0)
        self.mousebuttons= [0]*9
        self.quit = False
        self.char_stack = deque([])
        self.rec_char = False

    def update(self, display):
        for event in pygame.event.get():
            if event.type == KEYDOWN:
                self.key[event.key] = 1;
                if self.rec_char:
                    self.push_char(event.unicode)
            elif event.type == KEYUP:
                self.key[event.key] = 0;
            elif event.type == MOUSEMOTION:
                self.mouse = event.pos
                self.mouserel = event.rel
            elif event.type == MOUSEBUTTONDOWN:
                self.mousebuttons[event.button] = 1
            elif event.type == MOUSEBUTTONUP:
                if event.button != 4 and event.button != 5:
                    self.mousebuttons[event.button] = 0
            elif event.type == QUIT:
                self.quit = True
            elif event.type == VIDEORESIZE:
                display.resize(event.size)

    def push_char(self, c):
        self.char_stack.append(c)
        
    def get_key_once(self, k):
        if self.key[k]:
            self.key[k] = False
            return True
        else:
            return False
