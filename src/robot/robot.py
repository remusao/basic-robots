##
## robot.py for BASIC-RoBots in /home/surply_p
## 
## Made by Pierre Surply
## <pierre.surply@gmail.com>
## 
## Started on  Sun Jul  1 15:34:10 2012 Pierre Surply
## Last update Sat Jul 21 18:36:42 2012 Pierre Surply
##

import os
import random

import robotsos
import inventory

class Robot:
    def __init__(self, new, world, name, env, (x, y), events):
        self.env = env
        self.pos_x = x
        self.pos_y = y
        self.orient = 0
        self.id_sprite = 0
        self.name = name
        self.world = world
        self.events = events
        self.cannot_move = [2, 4, 6]
        self.energy = 255
        self.shield = 255
        self.inv = inventory.Inventory("saves/"+self.world+"/robots/"+self.name+"/inv", 10000)
        if new:
            self.new()
            self.save_pos()
        else:
            self.load_pos()
            self.load_prop()
            self.load_inventory()
        self.mem = {"A" : 0,\
                        "B" : 0,\
                        "C" : 0,\
                        "D" : 0,\
                        "E" : 0}
        self.ext_cmd = {"forward" : (self.forward, \
                                         "Move the robot forward", \
                                         "1 if the robot can move, 0 otherwise",\
                                         (1, []),\
                                         {}),\
                            "rotleft": (self.rot_left, \
                                            "Rotate the robot counterclockwise",\
                                            "Always returns 0", \
                                            (0, []),\
                                            {}),\
                            "rotright": (self.rot_right, \
                                             "Rotate the robot clockwise",\
                                             "Always returns 0", \
                                             (0, []),\
                                             {}),\
                            "give": (self.give, \
                                         "Give an item from inventory to other robot",\
                                         "1 if the item is given, 0 otherwise",\
                                         (0, []),\
                                         {"A" : "item id"})}
        self.os = robotsos.RoBotsOS(self, "saves/"+world+"/robots/"+self.name+"/fs", \
                                        self.mem, \
                                        events)

    def new(self):
        i = 0
        new_name = self.name + "_" + str(i)
        while os.path.exists("saves/"+self.world+"/robots/"+new_name):
            i += 1
            new_name = self.name + "_" + str(i)
        self.name = new_name
        os.makedirs("saves/"+self.world+"/robots/"+self.name+"/fs")
        self.inv.path = "saves/"+self.world+"/robots/"+self.name+"/inv"

    def get_pos(self):
        return (self.pos_x, self.pos_y)

    def update(self, selected, display, events):
        self.os.update(selected, display, events)

    def save_pos(self):
        f = open("saves/"+self.world+"/robots/"+self.name+"/pos", 'w')
        buf = str(self.pos_x) + "-" + str(self.pos_y) + "-" + str(self.orient)
        f.write(buf)
        f.close()

    def save_prop(self):
        f = open("saves/"+self.world+"/robots/"+self.name+"/prop", 'w')
        buf = str(self.energy)
        f.write(buf)
        f.close()
        
    def save(self):
        self.save_prop()
        self.save_pos()
        self.inv.save()

    def load_pos(self):
        if os.path.isfile("saves/"+self.world+"/robots/"+self.name+"/pos"):
            f = open("saves/"+self.world+"/robots/"+self.name+"/pos", 'r')
            buf = f.read().split("-")
            f.close()
            self.pos_x = int(buf[0])
            self.pos_y = int(buf[1])
            self.orient = int(buf[2])

    def load_prop(self):
        if os.path.isfile("saves/"+self.world+"/robots/"+self.name+"/prop"):
            f = open("saves/"+self.world+"/robots/"+self.name+"/prop", 'r')
            self.energy = int(f.read())

    def load_inventory(self):
        if os.path.isfile("saves/"+self.world+"/robots/"+self.name+"/inv"):
            f = open("saves/"+self.world+"/robots/"+self.name+"/inv", 'r')
            self.inv.parse(f.read())
            f.close()

    def coll(self, (x, y)):
        return not(x >= 0 and \
                    y >= 0 and \
                    x < self.env.size and\
                    y < self.env.size and\
                    self.env.tile_elts[x][y] == None and\
                    not self.env.tile[x][y] in self.cannot_move and\
                    not (x, y) in self.env.get_pos_robots())

    def run_ext_cmd(self, f, price):
        if self.energy > 0:
            result = f()
            if result != 0:
                self.incr_energy(-price[0])
            return result
        else:
            self.os.terminal.info.write_info("Not enough energy", 8)

    def incr_energy(self, incr):
        self.energy += incr
        if self.energy <= 0:
            self.energy = 0
            self.os.terminal.write("*** Out of energy *** \nStopping robot...\n", 6)
        elif self.energy < 50:
            self.os.terminal.write("Warning : the energy level is low\n", 5)
        elif self.energy > 255:
            self.energy = 255
        
    def move_to(self, (x, y)):
        if not self.coll((x, y)):
            self.pos_x = x
            self.pos_y = y
            self.save_pos()
            self.os.terminal.info.write_info("Moved to (" + str(x) + ", " + str(y) + ")", 7)
            return 1
        else:
            self.os.terminal.info.write_info("Can't move to (" + str(x) + ", " + str(y) + ")", 8)
            return 0

    def get_pos_forward(self):
        if self.orient == 0:
            return (self.pos_x, self.pos_y-1)
        elif self.orient == 1:
            return (self.pos_x+1, self.pos_y)
        elif self.orient == 2:
            return (self.pos_x, self.pos_y+1)
        elif self.orient == 3:
            return (self.pos_x-1, self.pos_y)

    def forward(self):
        if self.inv.isfull():
            self.os.terminal.info.write_info("The robot carries too much", 8)
            return 0
        else:
            return self.move_to(self.get_pos_forward())

    def rot_left(self):
        return self.rot(-1)

    def rot_right(self):
        return self.rot(1)
        
    def rot(self, incr):
        self.orient += incr
        if self.orient < 0:
            self.orient = 3
        elif self.orient > 3:
            self.orient = 0
        return 0

    def add_item(self, item, qty):
        if qty > 0 and item < len(self.inv.temp_items):
            temp = self.inv.temp_items[item]
            self.inv.set_item(item, qty)
            self.os.terminal.info.write_info("[INV] " + temp[0] + " collected (" + \
                                            str(qty) + "x" + str(temp[1]) + "g)", 1)
            if self.inv.isfull():
                self.os.terminal.info.write_info("Warning : The robot carries too much", 8)

    def add_item_rnd(self, item, qty, prob):
        if (random.randint(1, prob) == 1):
            self.add_item(item, random.randint(1, qty))

    def del_item(self, item, qty):
        if qty > 0 and item < len(self.inv.temp_items):
            temp = self.inv.temp_items[item]
            if self.inv.set_item(item, -qty):
                self.os.terminal.info.write_info("[INV] " + temp[0] + " removed (" + \
                                                str(qty) + "x" + str(temp[1]) + "g)", 1)
                return True
            else:
                return False
        else:
            return False

    def give(self):
        id_item = self.mem["A"]
        qty = 1
        pos = self.get_pos_forward()
        for i in self.env.robots:
            if i.get_pos() == pos and self.del_item(id_item, qty):
                i.add_item(id_item, qty)
                return 1
        return 0
