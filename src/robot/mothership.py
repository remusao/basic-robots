##
## mothership.py for BASIC-RoBots in /home/surply_p
## 
## Made by Pierre Surply
## <pierre.surply@gmail.com>
## 
## Started on  Sun Jul  1 16:42:11 2012 Pierre Surply
## Last update Mon Jul 23 12:02:37 2012 Pierre Surply
##

import os

import robot
import woodcutter
import tipper
import thermalpowerstation

class Mothership(robot.Robot):
    av_robots = [("Woodcutter", lambda o: \
                      woodcutter.Woodcutter(True,\
                                                o.world,\
                                                "Woodcutter_"+str(o.env.x)+"-"+str(o.env.y),\
                                                o.env,\
                                                (0,0),\
                                                o.events)),\
                     ("Tipper", lambda o: \
                          tipper.Tipper(True,\
                                            o.world,\
                                            "Tipper_"+str(o.env.x)+"-"+str(o.env.y),\
                                            o.env,\
                                            (0,0),\
                                            o.events)),\
                     ("Thermal Power Station", lambda o: \
                          thermalpowerstation.ThermalPowerStation(True,\
                                                                      o.world,\
                                                                      "TPS_"+str(o.env.x)+"-"+str(o.env.y),\
                                                                      o.env,\
                                                                      (0,0),\
                                                                      o.events))]
    def __init__(self, new, world, name, env, (x, y), events):
        robot.Robot.__init__(self, new, world, name, env, (x, y), events)
        self.id_sprite = 0
        self.ext_cmd["build"] = (self.build,\
                                     "Build a robot",\
                                     "1 if the robot can be created, 0 otherwise",\
                                     (2, []),\
                                     {"A" : "robot id"})
        self.ext_cmd["robots"] = (self.robots,\
                                      "List available robots",\
                                      "Always returns 0",\
                                      (0, []),\
                                      {})

    def robots(self):
        self.os.terminal.write("Id", 1)
        self.os.terminal.write("  ")
        self.os.terminal.write_line("Name", 1)
        for i in range(len(self.av_robots)):
            s_id = str(i)
            self.os.terminal.write_line(s_id + (" " * (4-len(s_id))) + self.av_robots[i][0])
        return 0

    def build(self):
        id_robot = self.mem["A"]
        x, y = self.get_pos_forward()
        if not self.coll(self.get_pos_forward()):
            r = self.id2robot(id_robot)
            if r != None:
                r.move_to((x, y))
                self.env.robots.append(r)
                self.os.terminal.info.write_info(r.name + " built in (" + str(x) + ", " + str(y) + ")", 7)
                return 1
            else:
                self.os.terminal.info.write_info("No robot with id " + str(id_robot), 7)           
                return 0
        else:
            self.os.terminal.info.write_info("Can't build in (" + str(x) + ", " + str(y) + ")", 8)
            return 0

    def id2robot(self, id_robot):
        if id_robot < len(self.av_robots):
            return self.av_robots[id_robot][1](self)
        else:
            return None
