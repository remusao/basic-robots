##
## basic.py for BASIC-RoBots in /home/surply_p
## 
## Made by Pierre Surply
## <pierre.surply@gmail.com>
## 
## Started on  Sat May  5 13:04:41 2012 Pierre Surply
## Last update Sun Jul 22 13:21:27 2012 Pierre Surply
##

import random
import time

class Basic:
    func = {"rand" : (lambda: random.randint(0, 255)),\
                "time" : (lambda: int(time.time()) % 256)}
    def __init__(self, current_dir, src, mem, mem_ro, robot, terminal):
        self.current_dir = current_dir
        self.src = src
        self.terminal = terminal
        self.mem = mem
        self.mem_ro = mem_ro
        self.robot = robot
        self.ext_cmd = self.robot.ext_cmd
        self.callstack = []
        self.whilestack = []
        self.opr = self.set_opr(self.src)
        self.line = 0

    def set_opr(self, src):
        opr = []
        for i in src.split("\n"):
            opr.append(filter(lambda x: x != "", i.split(" ")))
        return opr
            
    def eval_line(self):
        if self.line < len(self.opr):
            opr = self.opr[self.line]
            if len(opr) > 0:
                cmd = opr[0].upper()
                if cmd == "PRINT":
                    self.write(opr, self.line)
                elif cmd == "READ":
                    try:
                        self.line = self.read(opr[1], self.line)
                    except IndexError:
                        self.line = self.raise_error_arg(cmd, 1, self.line)
                elif cmd == "INCLUDE":
                    try:
                        self.line = self.include(self.current_dir + opr[1], self.line)
                    except IndexError:
                        self.line = self.raise_error_arg(cmd, 1, self.line)
                elif cmd == "LET":
                    try:
                        var = opr[1]
                        s = ""
                        for i in opr[2::]:
                            s += i + " "
                        self.line = self.let(var, s, self.line)
                    except IndexError:
                        self.line = self.raise_error_arg(cmd, 2, self.line)
                elif cmd == "GOTO":
                    try:
                        label = opr[1]
                        self.line = self.goto_label(label, self.line)
                    except IndexError:
                        self.line = self.raise_error_arg(cmd, 1, self.line)
                elif cmd == "GOTOSUB":
                    try:
                        label = opr[1]
                        self.callstack.append(self.line)
                        self.line = self.goto_label(label, self.line)
                    except IndexError:
                        self.line = self.raise_error_arg(cmd, 1, self.line)
                elif cmd == "RETURN":
                    if len(self.callstack) > 0 :
                        self.line = self.callstack.pop()
                elif cmd == "WHILE":
                    try:
                        self.line = self.while_loop_begin(opr[1::], self.line)
                    except IndexError:
                        self.line = self.raise_error_arg(cmd, 1, self.line)
                elif cmd == "ENDWHILE":
                    self.line = self.while_loop_end(self.line)
                elif cmd == "IF":
                    self.line = self.alt(opr[1::], self.line)
                elif cmd == "CALL":
                    try:
                        if len(opr) < 3:
                            self.line = self.call(opr[1], None, self.line)
                        else:
                            self.line = self.call(opr[1], opr[2], self.line)
                    except IndexError:
                        self.line = self.raise_error_arg(cmd, 1, self.line)
                elif not(cmd in ["LABEL", "REM"]):
                    self.line = self.raise_error("Unknown command \"" + cmd + "\"" , self.line)
            self.line += 1
            return True
        else:
            return False

    def write(self, opr, line):
        s = ""
        for i in opr[1::]:
            if len(i) > 1 and i[0] == '$':
                s += str(self.eval_exp(i[1::], line)) + " "
            else:
                s += i + " "
        self.terminal.write_line(s)

    def read(self, var, line):
        if var in self.mem:
            if not self.terminal.act_prompt:
                self.terminal.start_prompt("?")
            cmd = self.terminal.prompt()
            if cmd != None:
                try:
                    self.mem[var] = int(eval(cmd))
                except:
                    self.mem[var] = 0
                return line
            else:
                return line-1
        else:
            return self.raise_error("The robot hasn't the \"" + var + "\" register", line)

    def let(self, var, expr, line):
        if var in self.mem:
            self.set_mem(var, self.eval_exp(expr, line))
            return line
        else:
            return self.raise_error("The robot hasn't the \"" + var + "\" register", line)

    def set_mem(self, var, val):
        self.mem[var] = val
        if self.mem[var] > 255:
            self.mem[var] = 255
        elif self.mem[var] < 0:
            self.mem[var] = 0

    def goto_return(self, label, line):
        return self.goto(label, "GOTOSUB", line)

    def goto_label(self, label, line):
        return self.goto(label, "LABEL", line)

    def goto(self, label, cmd, line):
        i = 0
        while i < len(self.opr) and\
                (len(self.opr[i]) < 1  or \
                     (self.opr[i][0].upper() != cmd) or \
                     (self.opr[i][1] != label)):
            i += 1
        if i < len(self.opr):
            return i
        else:
            return self.raise_error("The " + cmd.lower() + " \"" + label +"\" can't be found", line)

    def alt(self, opr, line):
        boolean = []
        label_then = ""
        label_else = ""
        i=0
        while i < len(opr) and opr[i].upper() != "THEN":
            boolean.append(opr[i])
            i += 1
        if len(opr) <= i+1:
            return self.raise_error("Syntax error", line)
        else:
            i += 1
            label_then = opr[i]
            if len(opr) > i+1:
                i += 1
                if opr[i].upper() == "ELSE" and len(opr) > i+1:
                    label_else = opr[i+1]
                else:
                    return self.raise_error("Syntax error", line)
            if self.eval_boolean("".join(boolean), line):
                return self.goto_label(label_then, line)
            elif label_else != "":
                return self.goto_label(label_else, line)
            else:
                return line

    def while_loop_begin(self, opr, line):
        if self.eval_boolean("".join(opr), line):
            self.whilestack.append(line)
            return line
        else:
            nbr_loop = 0
            i = line+1
            while i < len(self.opr) and\
                    (len(self.opr[i]) < 1  or \
                         (self.opr[i][0].upper() != "ENDWHILE") or \
                         (nbr_loop > 0)):
                if len(self.opr[i]) >= 1:
                    cmd = self.opr[i][0].upper()
                    if (cmd == "WHILE"):
                        nbr_loop += 1
                    elif (cmd == "ENDWHILE"):
                        nbr_loop -= 1
                i += 1
            if i < len(self.opr):
                return i
            else:
                return self.raise_error("", line)

    def while_loop_end(self, line):
        return self.whilestack.pop()-1

    def eval_exp(self, exp, line):
        try:
            return int(eval(exp, {"__builtins__":None}, dict(self.mem, \
                                                                 **dict(self.func, **self.mem_ro))))
        except:
            self.raise_warning("Invalid Expression", line)
            return 0

    def eval_boolean(self, exp, line):
        try:
            return eval(exp, {"__builtins__":None}, dict(self.mem, **self.func))
        except:
            self.raise_warning("Invalid Boolean", line)
            return False

    def include(self, path, line):
        try:
            src = open(path, 'r').read()
            self.opr = self.opr[0:line] + self.set_opr(src) + self.opr[line+1:]
            return line-1
        except IOError:
            return self.raise_error("The program " + path + " can't be found", line)
        
    def call(self, function, reg, line):
        if function in self.ext_cmd:
            if reg == None:
                self.ext_cmd[function][0]()
                return line
            else:
                if reg in self.mem:
                    self.set_mem(reg, self.robot.run_ext_cmd(self.ext_cmd[function][0], \
                                                                 self.ext_cmd[function][3]))
                    return line
                else:
                    return self.raise_error("The robot hasn't the \"" + var + "\" register", line)
        else:
            return self.raise_error("The external command \""+ function + "\"is not defined for this robot", line)

    def raise_error_arg(self, cmd, nbr_arg, line):
        self.raise_error("The command \"" + cmd + "\" expect " + str(nbr_arg) + " argument(s)", line)
        
    def raise_error(self, msg, line):
        self.terminal.write_line("** Error **", 6)
        self.terminal.write_line("** Line " + str(line + 1) + ": " + msg, 6)
        return len(self.opr)

    def raise_warning(self, msg, line):
        self.terminal.write_line("** Warning **", 5)
        self.terminal.write_line("** Line " + str(line + 1) + ": " + msg, 5)
