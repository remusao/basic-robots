##
## os.py for BASIC-RoBots in /home/surply_p
## 
## Made by Pierre Surply
## <pierre.surply@gmail.com>
## 
## Started on  Wed May  9 22:56:00 2012 Pierre Surply
## Last update Sun Jul 22 10:46:32 2012 Pierre Surply
##

import os
from pygame.locals import *

os.chdir('src/robot')
import terminal
os.chdir('../../')
import basic

class Command:
    def __init__(self, name, param, description, function):
        self.name = name
        self.param = param
        self.description = description
        self.function = function

    def apply(self, os, arg):
        self.function(os, arg)

class RoBotsOS:
    def __init__(self, robot, path, mem, events):
        self.robot = robot
        self.current_dir = "/"
        self.path = path
        self.name = robot.name
        self.ext_cmd = robot.ext_cmd
        self.mem = mem
        self.terminal = terminal.Terminal(events)
        self.terminal.set_title(self.name)
        self.set_cmds([("help", "", "show help", lambda o, a:o.help(a)),\
                           ("info", "", "show some information about the robot", lambda o, a:o.info(a)),\
                           ("intcmd", "", "show internal commands", lambda o, a: o.show_int_cmd(a)),\
                           ("extcmd", "", "show external commands", lambda o, a: o.show_ext_cmd(a)),\
                           ("welcome", "", "print welcome message", lambda o, a: o.welcome(a)),\
                           ("pwd", "", "print name of current directory", lambda o, a: o.pwd(a)),\
                           ("ls", "", "list directory contents", lambda o, a: o.ls(a)),\
                           ("mkdir", "DIRECTORY", "make directory", lambda o, a: o.mkdir(a)),\
                           ("touch", "FILE", "make file", lambda o, a: o.touch(a)),\
                           ("cd", "[DIRECTORY]", "change current directory", lambda o, a: o.cd(a)),\
                           ("cat", "FILE", "print file", lambda o, a: o.cat(a)),\
                           ("run", "FILE", "execute a BASIC script", lambda o, a: o.execute(a)),\
                           ("edit", "FILE", "edit a file", lambda o, a: o.edit(a)),\
                           ("inv", "", "list inventory", lambda o, a: o.ls_inv(a)),\
                           ("let", "REG VALUE", "set register value", lambda o, a: o.let(a)),\
                           ("clear", "", "clear screen", lambda o, a: o.clear(a))])
        if self.robot.energy > 0:
            self.welcome(None)
        self.terminal.start_prompt(">")
        self.task = 0

    def get_path(self, path):
        return self.path + path

    def set_cmds(self, cmds):
        self.cmds = []
        for i in cmds:
            self.cmds.append(Command(i[0], i[1], i[2], i[3]))

    def update(self, selected, display, events):
        w = display.window.get_width()/16-1
        h = display.window.get_height()/12-1
        if self.robot.energy > 0:
            if self.task == 0 and selected:
                cmd = self.terminal.prompt()
                if cmd != None:
                    self.command(cmd)
                    if self.task == 0 and self.robot.energy > 0:
                        self.terminal.start_prompt(">")
                info = (self.mem, 8)
            elif self.task == 1:
                if not self.basic.eval_line() or \
                        (selected and events.key[K_LCTRL] and \
                             events.get_key_once(K_c)):
                    self.task = 0
                    if self.robot.energy > 0:
                        self.terminal.start_prompt(">")
                info = (self.mem, 8)
            elif self.task == 2 and selected:
                if not self.terminal.edit(self, h):
                    self.task = 0
                    self.terminal.start_prompt(">")
                info = ({"CTRL+S" : "Save", \
                             "CTRL+C" : "Close"}, 15)
        else:
            if selected:
                while len(events.char_stack) > 0:
                    events.char_stack.popleft()
            info = None
        if selected:
            self.terminal.display(display, (0, 0, w, h), info, ({"SH" : self.robot.shield,\
                                                                     "EN" : self.robot.energy}, 8))

    def command(self, cmd):
        cmd = cmd.strip().split(" ")
        found = False
        for i in self.cmds:
            if cmd[0] == i.name:
                i.apply(self, cmd[1:])
                found = True
        if not found:
            if cmd[0] in self.ext_cmd:
                self.robot.run_ext_cmd(self.ext_cmd[cmd[0]][0], \
                                           self.ext_cmd[cmd[0]][3])
                found = True
        if not found and cmd[0] != "":
            self.terminal.write_line("'" + cmd[0] + "' : command not found")

    def info(self, arg):
        self.terminal.write("--- Informations ---\n", 1)
        self.terminal.write("Name", 1)
        self.terminal.write_line(": " + self.name)
        self.terminal.write("X", 1)
        self.terminal.write(": " + str(self.robot.pos_x) + "  ")
        self.terminal.write("Y", 1)
        self.terminal.write(": " + str(self.robot.pos_y) + "  ")
        self.terminal.write("Or", 1)
        self.terminal.write_line(": " + ["North", "East", "South", "West"][self.robot.orient])
        self.terminal.write("Shield", 1)
        self.terminal.write_line(": " + str(self.robot.shield) + "%")
        self.terminal.write("Energy", 1)
        self.terminal.write_line(": " + str(self.robot.energy) + "%")

    def help(self, arg):
        if len(arg) > 0:
            if arg[0] in self.ext_cmd:
                self.help_cmd(arg[0], True, self.ext_cmd[arg[0]][1], \
                                  self.ext_cmd[arg[0]][4], \
                                  self.ext_cmd[arg[0]][3], \
                                  self.ext_cmd[arg[0]][2])
            else:
                for i in self.cmds:
                    if arg[0] == i.name:
                        self.help_cmd(i.name, False, i.description, i.param)
        else:
            self.show_int_cmd(None)
            self.show_ext_cmd(None)

    def help_cmd(self, name, ext, desc, arg, cost=None, ret=None):
        self.terminal.write("Command", 1)
        self.terminal.write_line(": " + name)
        self.terminal.write("Type", 1)
        if ext:
            self.terminal.write_line(": external command")
        else:
            self.terminal.write_line(": internal command")
            self.terminal.write("Argument(s)", 1)
            self.terminal.write_line(": " + arg)
        self.terminal.write("Description", 1)
        self.terminal.write_line(": " + desc)
        if ext:
            self.terminal.write("Argument(s)", 1)
            if arg == {}:
                self.terminal.write_line(": -")
            else:
                self.terminal.write(":\n")
                for i in arg.keys():                    
                    self.terminal.write_line("  \x10 " + i + ": " + arg[i])
            self.terminal.write("Return", 1)
            self.terminal.write_line(": " + ret)
            self.terminal.write("Cost", 1)
            self.terminal.write_line(": " + str(cost[0]) + " EN")

    def show_int_cmd(self, arg):
        self.terminal.write("--- Internal commands ---\n", 1)
        for i in self.cmds:
            self.terminal.write(i.name, 1)
            self.terminal.write(" " + i.param + ": " + i.description + "\n")

    def show_ext_cmd(self, arg):
        self.terminal.write("--- External commands ---\n", 1)
        for i in sorted(self.ext_cmd.keys()):
            self.terminal.write(i, 1)
            self.terminal.write(": " + self.ext_cmd[i][1] + "\n")
                
    def welcome(self, arg):
        self.terminal.write_line("*** " + self.name +  " ***\nType 'help' for more information")

    def pwd(self, arg):
        self.terminal.write_line(self.current_dir)

    def ls(self, arg):
        s = ""
        for i in os.listdir(self.get_path(self.current_dir)):
            s += i + " "
        self.terminal.write_line(s)

    def mkdir(self, arg):
        if len(arg) > 0:
            os.mkdir(self.get_path(self.current_dir + arg[0]))

    def touch(self, arg):
        if len(arg) > 0:
            path = self.get_path(self.current_dir + arg[0])
            if not os.path.exists(path): 
                open(path, 'w').close() 
    
    def cd(self, arg):
        directory = "/"
        if len(arg) > 0:
            if arg[0][0] == "/":
                directory = arg[0]
            elif arg[0] == "..":
                if len(self.current_dir) > 2:
                    directory = "/".join(self.current_dir.split("/")[:-2])
            else:
                directory = self.current_dir+arg[0]
        if os.path.exists(self.get_path(directory)):
            if os.path.isdir(self.get_path(directory)):
                self.current_dir = directory
                if len(self.current_dir) == 0 or self.current_dir[-1] != "/":
                    self.current_dir += "/"
            else:
                self.terminal.write_line(arg[0] + ": Not a directory")
        else:
            self.terminal.write_line(arg[0] + ": No such file or directory")
        
    def clear(self, arg):
        self.terminal.clear()

    def cat(self, arg):
        if len(arg) > 0:
            if os.path.isfile(self.get_path(self.current_dir + arg[0])):
                self.terminal.write_line(open(self.get_path(self.current_dir + arg[0]), 'r').read())
            else:
                self.terminal.write_line(arg[0] + ": No such file or directory")

    def execute(self, arg):
        if len(arg) > 0:
            if os.path.isfile(self.get_path(self.current_dir + arg[0])):
                f = open(self.get_path(self.current_dir) + arg[0], 'r')
                self.basic = basic.Basic(self.get_path(self.current_dir),\
                                             f.read(), \
                                             self.mem,\
                                             {"SH" : self.robot.shield,\
                                                  "EN" : self.robot.energy,\
                                                  "X" : self.robot.pos_x,\
                                                  "Y" : self.robot.pos_y},\
                                             self.robot,\
                                             self.terminal)
                f.close()
                self.terminal.write_line("Running " + arg[0] + "...\nPress CTRL+C to interrupt")
                self.task = 1
            else:
                self.terminal.write_line(arg[0] + ": No such file or directory")
        else:
            self.terminal.write_line("run: No input file")

    def edit(self, arg):
        if len(arg) > 0:
            if os.path.isfile(self.get_path(self.current_dir + arg[0])):
                self.terminal.start_edit(self.current_dir + arg[0], \
                                             open(self.get_path(self.current_dir+arg[0])).read())
                self.task = 2
            else:
                self.terminal.write_line(arg[0] + ": No such file or directory")
        else:
            self.terminal.write_line("edit: No input file")

    def save_file(self, path, s):
        f = open(self.get_path(path), 'w')
        f.write("\n".join(s))

    def ls_inv(self, arg):
        if self.robot.inv.weight > 0:
            self.terminal.write("Id", 1)
            self.terminal.write("  ")
            self.terminal.write("Item", 1)
            self.terminal.write("        ")
            self.terminal.write("Qty\n", 1)
            self.terminal.write_line(str(self.robot.inv))
        else:
            self.terminal.write("\n --Empty--\n")
        self.terminal.write("\nCapacity", 1)
        self.terminal.write_line(": " + str(self.robot.inv.capacity) + " g")
        self.terminal.write("Weight", 1)
        self.terminal.write_line(": " + str(self.robot.inv.weight) + " g")
        self.terminal.write("Free", 1)
        f = self.robot.inv.capacity - self.robot.inv.weight
        self.terminal.write(": ")
        if f > 0:
            self.terminal.write(str(f))
        else:
            self.terminal.write(str(f), 6)
        self.terminal.write(" g (" + str((f*100)/self.robot.inv.capacity) + " %)\n")

    def let(self, arg):
        self.basic = basic.Basic(self.get_path(self.current_dir),\
                                     "LET " + " ".join(arg), \
                                     self.mem,\
                                     {"SH" : self.robot.shield,\
                                          "EN" : self.robot.energy,\
                                          "X" : self.robot.pos_x,\
                                          "Y" : self.robot.pos_y},\
                                     self.robot,\
                                     self.terminal)
        self.task = 1
