##
## thermalpowerstation.py for BASIC-RoBots in /home/surply_p
## 
## Made by Pierre Surply
## <pierre.surply@gmail.com>
## 
## Started on  Mon Jul 23 11:55:10 2012 Pierre Surply
## Last update Mon Jul 23 12:19:20 2012 Pierre Surply
##

import robot

class ThermalPowerStation(robot.Robot):
    def __init__(self, new, world, name, env, (x, y), events):
        robot.Robot.__init__(self, new, world, name, env, (x, y), events)
        self.inv.capacity = 8000000
        self.id_sprite = 3
