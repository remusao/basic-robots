##
## woodcutter.py for BASIC-RoBots in /home/surply_p
## 
## Made by Pierre Surply
## <pierre.surply@gmail.com>
## 
## Started on  Tue Jul  3 14:38:15 2012 Pierre Surply
## Last update Sat Jul 21 16:58:30 2012 Pierre Surply
##

import robot

class Woodcutter(robot.Robot):
    def __init__(self, new, world, name, env, (x, y), events):
        robot.Robot.__init__(self, new, world, name, env, (x, y), events)
        self.inv.capacity = 100000
        self.id_sprite = 1
        self.ext_cmd["cuttree"] = (self.cut_wood,\
                                       "Cut a tree to make wood",\
                                       "1 if a tree has been cut, 0 otherwise", \
                                       (2, []), \
                                       {})

    def cut_wood(self):
        pos = self.get_pos_forward()
        if self.env.tile_elts[pos[0]][pos[1]] == 0:
            self.env.tile_elts[pos[0]][pos[1]] = None
            self.add_item(0, 8)
            self.add_item_rnd(1, 2, 2)
            return 1
        else:
            return 0
