##
## tipper.py for BASIC-RoBots in /home/surply_p
## 
## Made by Pierre Surply
## <pierre.surply@gmail.com>
## 
## Started on  Sat Jul 21 13:47:50 2012 Pierre Surply
## Last update Mon Jul 23 12:19:47 2012 Pierre Surply
##

import robot

class Tipper(robot.Robot):
    def __init__(self, new, world, name, env, (x, y), events):
        robot.Robot.__init__(self, new, world, name, env, (x, y), events)
        self.inv.capacity = 18000000
        self.id_sprite = 2
