#!/usr/bin/python
## savable.py for BASIC-RoBots in /home/surply_p
## 
## Made by Pierre Surply
## <pierre.surply@gmail.com>
## 
## Started on  Sat Jun 30 15:34:37 2012 Pierre Surply
## Last update Sat Jun 30 17:42:43 2012 Pierre Surply
##

import pygame
from pygame.locals import *

class Savable:
    def save_img(self, path, size, tile, function):
        surface = pygame.Surface((size, size))
        for y in range(size):
            for x in range(size):
                surface.set_at((x, y), function((x, y)))
        pygame.image.save(surface, path)

    def load_img(self, path, size, tile, function):
        surface = pygame.image.load(path)
        for y in range(min(size, surface.get_height())):
            for x in range(min(size, surface.get_width())):
                tile[x][y] = function(tuple(surface.get_at((x, y))))
