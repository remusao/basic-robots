#!/usr/bin/python
## surface.py for BASIC-RoBots in /home/surply_p
## 
## Made by Pierre Surply
## <pierre.surply@gmail.com>
## 
## Started on  Thu Jun 28 13:23:22 2012 Pierre Surply
## Last update Sun Jul  1 19:14:34 2012 Pierre Surply
##

import pygame
from pygame.locals import *

def cut_surface(surface, (w, h)):
    l_surface = []
    for y in range(surface.get_height() / h):
        for x in range(surface.get_width() / w):
            new = pygame.Surface((w, h))
            new.blit(surface, (0, 0), pygame.Rect(x*w, y*h, w, h))
            new.set_colorkey(pygame.Color(255, 0, 255, 255))
            l_surface.append(new)
    return l_surface
    
def change_color(surface, front, back = pygame.Color(255, 0, 255, 255)):
    new_surface = surface.copy()
    for y in range(new_surface.get_height()):
        for x in range(new_surface.get_width()):
            c = tuple(new_surface.get_at((x, y)))
            if (c == (255, 255, 255, 255)):
                new_surface.set_at((x, y), front)
            elif (c == (255, 0, 255, 255)):
                new_surface.set_at((x, y), back)
    return new_surface

def rotate(surface):
    return [surface, \
                pygame.transform.rotate(surface, -90),\
                pygame.transform.flip(surface, False, True), \
                pygame.transform.rotate(surface, 90)]

def cut_rotate(surface, (w, h)):
    return [rotate(i) for i in cut_surface(surface, (w, h))]
