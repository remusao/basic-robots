##
## dashboard.py for BASIC-RoBots in /home/surply_p
## 
## Made by Pierre Surply
## <pierre.surply@gmail.com>
## 
## Started on  Thu Jun 28 21:12:48 2012 Pierre Surply
## Last update Tue Jul  3 11:57:56 2012 Pierre Surply
##

import pygame
from pygame.locals import *

import worldmap
import terminal

class Dashboard:
    commands = [("ENTER", "land at the selected place"),\
                    (chr(24)+chr(25)+chr(26)+chr(27), "move cursor"),\
                    ("ESC", "cancel")]
    def __init__(self, worldmap, events):
        self.worldmap = worldmap
        self.terminal = terminal.Terminal(events)
        self.terminal.set_title("Worldmap")
        self.select_x = 16
        self.select_y = 16
        self.nbr_mothership = 1
        self.update_term()

    def update(self, display, events):
        self.handle_events(events)
        w = display.window.get_width()
        h = display.window.get_height()
        self.terminal.display(display, (0, 0, w/16-1, h/12-1))
        display.render_terminal()
        self.worldmap.render(display.window, \
                                    (w/2+((w/2 - 256) / 2), 16),\
                                 (self.select_x, self.select_y))
        display.flip()
        return events.get_key_once(K_RETURN)

    def handle_events(self, events):
        if events.get_key_once(K_UP) and self.select_y > 0:
            self.select_y -= 1
            self.update_term()
        if (events.get_key_once(K_DOWN)) and self.select_y < self.worldmap.size-1:
            self.select_y += 1
            self.update_term()
        if events.get_key_once(K_LEFT) and self.select_x > 0:
            self.select_x -= 1
            self.update_term()
        if (events.get_key_once(K_RIGHT)) and self.select_x < self.worldmap.size-1:
            self.select_x += 1
            self.update_term()

    def update_term(self):
        self.terminal.clear()
        self.terminal.write("Please select a landing site for the mothership\n\n")
        self.info_biomes(self.worldmap.tile[self.select_x][self.select_y])
        self.terminal.write("\n\n")
        self.info_commands()

    def info_biomes(self, n):
        self.terminal.write("World\n", 1)
        self.terminal.write("  ")
        self.terminal.write("Name",1)
        self.terminal.write_line(": " + self.worldmap.path)
        self.terminal.write("  ")
        self.terminal.write("Mothership",1)
        self.terminal.write(": " + str(self.nbr_mothership))
        self.terminal.write("\n\nSelected place\n", 1)
        for i in self.worldmap.biomes:
            if i[1] == n:
                self.terminal.write("  ")
                self.terminal.write("X", 1)
                self.terminal.write(": " + str(self.select_x))
                self.terminal.write("  ")
                self.terminal.write("Y", 1)
                self.terminal.write(": " + str(self.select_y))
                self.terminal.write("\n  ")
                self.terminal.write("Biome", 1)
                self.terminal.write(": " + i[2])
                self.terminal.write("\n  ")
                self.terminal.write("Difficulty", 1)
                if i[3] == 0:
                    dif = ("Very easy", 2)
                elif i[3] == 1:
                    dif = ("Easy", 3)
                elif i[3] == 2:
                    dif = ("Normal", 4)
                elif i[3] == 3:
                    dif = ("Hard", 5)
                elif i[3] == 4:
                    dif = ("Very hard", 6)
                self.terminal.write(": ")
                self.terminal.write(dif[0], dif[1])

    def info_commands(self):
        self.terminal.write("Commands\n", 1)
        for i in self.commands:
            self.terminal.write("  ")
            self.terminal.write(i[0], 1) 
            self.terminal.write(": " + i[1] + "\n")
